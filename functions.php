<?php

date_default_timezone_set('Asia/Karachi');
$today = date("Y-m-d");
/**
 * Converting a UNIX Timestamp to Formatted Date String
 */
function unixToDate($timestamp){
    $timezone  = +5;
    return gmdate("d M Y H:i", $timestamp + 3600*($timezone+date("I")));
}

function unxtodateF($timestamp, $format){
    $timezone  = +5;
    return gmdate($format, $timestamp + 3600*($timezone+date("I")));
}
/**
 * Convert kelvin to celsius
 */
function kelvinToCelsius($given_value)
{
	$celsius=$given_value-273.15;
	return round($celsius, 1) ;
}
/**
 * Convert Fahrenheit to celsius
 */
function FahrenheitToDegree($given_value)
{
    $v = ($given_value - 32) * 0.5556;
    return round($v, 1);
}
/**
 * To change date format 
 */
function toDateFromat($dates, $formats){
    $date=date_create($dates);
    return date_format($date, $formats);
}


/**
 * Select weather icon based on weather condition id by the API
 */
function weatherIcon($id){
    //cloudy
    if($id == 'cloudy' || $id == 'partly-cloudy-day' || $id == 'partly-cloudy-night'){
        echo '<div class="icon cloudy"><div class="cloud"></div><div class="cloud"></div></div>';
    }
    //light rain
    if($id >= 500 && $id <= 501){
        echo '<div class="icon sun-shower"><div class="cloud"></div><div class="sun"><div class="rays"></div></div><div class="rain"></div></div>';
    }
    //rain
    if($id >= 502 && $id <= 531){
        echo '<div class="icon rainy"><div class="cloud"></div><div class="rain"></div></div>';
    }
    //thunderstorm with rain  
    if($id >= 200 && $id <= 202){
        echo '<div class="icon rainy"><div class="cloud"></div><div class="rain"></div></div>';
        echo '<div class="icon thunder-storm"><div class="cloud"></div><div class="lightning"><div class="bolt"></div><div class="bolt"></div></div></div>';
    }
    //thunderstorm  
    if($id >= 210 && $id <= 232){
        echo '<div class="icon thunder-storm"><div class="cloud"></div><div class="lightning"><div class="bolt"></div><div class="bolt"></div></div></div>';
    }
    //clear sky
    if($id == 800){
        echo '<div class="icon sunny"><div class="sun"><div class="rays"></div></div></div>';
    }
    

}

/**
 * Covert degree to direction
 * taken from https://gist.github.com/smallindine
 */
function windCardinals($deg) {
	$cardinalDirections = array(
		'N' => array(348.75, 360),
		'N' => array(0, 11.25),
		'NNE' => array(11.25, 33.75),
		'NE' => array(33.75, 56.25),
		'ENE' => array(56.25, 78.75),
		'E' => array(78.75, 101.25),
		'ESE' => array(101.25, 123.75),
		'SE' => array(123.75, 146.25),
		'SSE' => array(146.25, 168.75),
		'S' => array(168.75, 191.25),
		'SSW' => array(191.25, 213.75),
		'SW' => array(213.75, 236.25),
		'WSW' => array(236.25, 258.75),
		'W' => array(258.75, 281.25),
		'WNW' => array(281.25, 303.75),
		'NW' => array(303.75, 326.25),
		'NNW' => array(326.25, 348.75)
	);
	foreach ($cardinalDirections as $dir => $angles) {
			if ($deg >= $angles[0] && $deg < $angles[1]) {
				$cardinal = $dir;
			}
		}
		return $cardinal;
}

function validate($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    $data = htmlentities($data, ENT_QUOTES);
    return $data;
}

?>