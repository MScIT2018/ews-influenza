<?php
include ('classes/Database.php');
include ('functions.php');

//Display alert list
$db = new Database(DB_HOST, DB_USER, DB_PASS, DB_NAME);
$alertlist = $db->query("SELECT alert_id, alert_date, alert_location FROM public.influenza_alert WHERE alert_date >= '$today' ORDER BY created_date DESC LIMIT 7");
$alertlist = $db->execute();
$alertlist = $db->resultset();
$maploc = '';
$mapLocArry = array();
foreach($alertlist as $nrow){
    $loc = $nrow['alert_location'];

    if($loc == 'Male'){
        $maploc =  "['Male', 4.172430, 73.509708]";
    }
    if($loc == 'Gan'){
        $maploc = "['Gan', -0.396948, 73.216170]";
    }
    if($loc == 'Hanimaadhoo'){
        $maploc = "['Hanimaadhoo', 6.751979, 73.164259]";
    }
    array_push($mapLocArry, $maploc);
}

$mapLocFinal = implode (", ", $mapLocArry);




function frocastGet($locations, $type){
    $today = date("Y-m-d");
    $db = new Database(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    $sql = $db->query("SELECT wfdate, humidity, temperature FROM public.weather_forcast WHERE wfdate >= '$today' AND wlocation = '".$locations."' ORDER BY wfdate LIMIT 7");
    $sql = $db->execute();
    $sql = $db->resultset();
    
    $humidityArrys = array();
    $temperatureArrys = array();
    
    $charArr = array();
    foreach($sql as $row){

        $ardate = $row['wfdate'];
        $arValue = $row['humidity'];
        $aarc = array($charArr[$ardate]=$arValue);
        array_merge($charArr, $aarc);


        array_push($humidityArrys, $row['humidity']);
        array_push($temperatureArrys, $row['temperature']);
    }

    
    
    if($type == 'humidity'){
        return implode(", ", $humidityArrys);
    }
    if($type == 'temperature'){
        return implode(", ", $temperatureArrys);
    }
}

// Start date
$date = date("Y-m-d");
$start_date = date('Y-m-d', strtotime($date. ' + 1 day'));
// End date
$end_date = date('Y-m-d', strtotime($date. ' + 6 days'));
$dateArray =array();
while (strtotime($date) <= strtotime($end_date)) {
    array_push($dateArray, toDateFromat($date, 'Y-m-d'));
    $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
}
$chartDates = "'".implode( "', '", $dateArray)."'";   

function chartNodes($loction){
    $db = new Database(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    $alerts = $db->query("SELECT alert_date, alert_location FROM public.influenza_alert WHERE alert_location = '".$loction."' ORDER BY created_date DESC LIMIT 7");
    $alerts = $db->execute();
    $alerts = $db->resultset();
    $alertArry = array();
    foreach($alerts as $rows){
        array_push($alertArry, $rows['alert_date']);
    }
        // Start date
        $date = date("Y-m-d");
        $start_date = date('Y-m-d', strtotime($date. ' + 1 day'));
        // End date
        $end_date = date('Y-m-d', strtotime($date. ' + 6 days'));
        $dateArray =array();
        while (strtotime($date) <= strtotime($end_date)) {
            array_push($dateArray, toDateFromat($date, 'Y-m-d'));
            $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
        }
        $chartDates = "'".implode( "', '", $dateArray)."'";        
        $result=array_intersect($dateArray,$alertArry);
        $alertPosi = (array_keys($result));
        foreach($alertPosi as $posi){
            $posi = $posi +1;
        }
        $alertPosiFlp = array_flip($alertPosi);
        $nodeArray = [0=>4, 1=>4, 2=>4, 3=>4, 4=>4, 5=>4, 6=>4];
        $alertNewArr = array();
        foreach($alertPosiFlp as $key=>$val){
            $val = 10;
            $alertNewArr[$key] = $val;
        }   
        $newChartNods = array_replace($nodeArray,$alertNewArr);   
        return implode(", ", $newChartNods);

}



?>

<section class="content-header">
      <h1>
      Early Warning System for influenza 
      </h1>
</section>

    <!-- Main content -->
    <section class="content container-fluid">
    <section class="col-lg-7 connectedSortable ui-sortable">

    <!--influenza warnings-->
        <div class="box box-info">
        <div class="box-header ui-sortable-handle" style="cursor: move;">
              <i class="fa fa-envelope"></i>

              <h3 class="box-title">Alerts</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                  <i class="fa fa-times"></i></button>
              </div>
              <!-- /. tools -->
            </div>
            
            <div class="box-body"> 
                         
                <div class="callout callout-warning">
                <h4>Possible influenza breakouts</h4>
                <?php 
                if(count($alertlist) < 1){
                    echo "Currently there is No influenza alert";
                }
                foreach($alertlist as $rows): 

                ?>  
                <div class="pull-left">
                    
                        <div id="ntdate" class="ntdate<?=  $rows['alert_id'] ?>">Date: <?= $rows['alert_date'] ?></div>
                        <div id="ntlocation" class="ntlocation<?=  $rows['alert_id'] ?>">Location: <?= $rows['alert_location'] ?></div>
                    </div>
                <button type="button" data-id="<?=  $rows['alert_id'] ?>" class="btn btn-info alert_share pull-right"><i id="prg<?=  $rows['alert_id'] ?>" class="fa fa-spin fa-refresh share_prgs hidex"></i> Share <i class="fa fa-telegram" aria-hidden="true"></i>
</button>
                <div class="clear"></div>
                <hr>
                <?php endforeach; ?>
              </div>
            

<canvas id="myChart"></canvas>

<script>
var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: [<?= $chartDates ?>],
        datasets: [{
            label: "Humidity (Central)",
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: '#ff0000',
            fill: false,
            pointRadius: [<?= chartNodes('Male') ?>],
            data: [<?= frocastGet('Male', 'humidity') ?>]
        },{
            label: "Temperature (Central in ℉)",
            data:  [<?= frocastGet('Male', 'temperature') ?>],
            fill: false,
            borderDash: [5, 5],
			borderColor: "#ff0000",
			backgroundColor: "#fff",
			pointBorderColor: "red",
			pointBackgroundColor: "red"
        },{
            label: "Humidity (South)",
            backgroundColor: '#0000ff',
            borderColor: '#0000ff',
            fill: false,
            pointRadius: [<?= chartNodes('Gan') ?>],
            data: [<?= frocastGet('Gan', 'humidity') ?>]
        },{
            label: "Temperature (South in ℉)",
            data:  [<?= frocastGet('Gan', 'temperature') ?>],
            fill: false,
            borderDash: [5, 5],
			borderColor: "#0000ff",
			backgroundColor: "#fff",
			pointBorderColor: "#0000ff",
			pointBackgroundColor: "#0000ff"
        },{
            label: "Humidity (North)",
            backgroundColor: '#00cc00',
            borderColor: '#00cc00',
            fill: false,
            pointRadius: [<?= chartNodes('Hanimaadhoo') ?>],
            data: [<?= frocastGet('Hanimaadhoo', 'humidity') ?>]
        },{
            label: "Temperature (North in ℉)",
            data:  [<?= frocastGet('Hanimaadhoo', 'temperature') ?>],
            fill: false,
            borderDash: [5, 5],
			borderColor: "#00cc00",
			backgroundColor: "#fff",
			pointBorderColor: "#00cc00",
			pointBackgroundColor: "#00cc00"
        }
    
        ]
        
    },

    // Configuration options go here
    options: {}
});
</script>



              
            </div>
        </div>
    <!-- current weather ifon -->
        <div class="box box-solid bg-light-blue-gradient" style="position: relative; left: 0px; top: 0px;">
          <div class="box-header ui-sortable-handle" style="cursor: move;">
            <i class="fa fa-cloud"></i>

            <h3 class="box-title">Weather</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-primary  btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-primary  btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
            </div>
            <!--Load live weather data to this div -->
            <div id="load_live_weather"></div>
          <!-- /.box-footer -->
        </div>




    </section>

    <section class="col-lg-5 connectedSortable ui-sortable">
    <div class="box box-solid">
            <div class="box-header ui-sortable-handle" style="cursor: move;">
              <!-- tools box -->
              <div class="pull-right box-tools">
              <button type="button" class="btn btn-primary  btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-primary  btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
              <!-- /. tools -->

              <i class="fa fa-map-marker"></i>

              <h3 class="box-title">
                Alert Map
              </h3>
            </div>
            <div class="box-body">
                <div id="map" style="width:100%; height:550px"></div>
            </div>
          </div>
    </section>

    </section>


<script>
function myMaps() {
var locations = [
<?= $mapLocFinal ?>
];
var map = new google.maps.Map(document.getElementById('map'), {
zoom: 6,
center: new google.maps.LatLng(3.335428, 73.155979),
mapTypeId: 'satellite',
mapTypeId: google.maps.MapTypeId.ROADMAP
});
var infowindow = new google.maps.InfoWindow();
var marker, i;
for (i = 0; i < locations.length; i++) {
marker = new google.maps.Marker({
position: new google.maps.LatLng(locations[i][1], locations[i][2]),
    icon: {
        path: google.maps.SymbolPath.CIRCLE,
        scale: 50,
        fillColor: "#F00",
        fillOpacity: 0.6,
        strokeWeight: 0.0
    },

map: map
});



google.maps.event.addListener(marker, 'click', (function(marker, i) {
return function() {
infowindow.setContent(locations[i][0]);
infowindow.open(map, marker);
}
})(marker, i));
}

}

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDN0HxMfQUwj0HkJ4eDwrhOpmA8xkWJR1k&callback=myMaps"></script>

