<?php
//give the full path of the server
//like /var/www/html
include ('../config.php');
include ('../classes/Database.php');
include ('../functions.php');
/**
* Get live weather data from darksky.net using free API
*/

forcast('https://api.darksky.net/forecast/d0478d169d05248ed9d50a7c926d5beb/4.1755,73.5093', 'Male');
forcast('https://api.darksky.net/forecast/d0478d169d05248ed9d50a7c926d5beb/-0.6883,73.1506', 'Gan');
forcast('https://api.darksky.net/forecast/d0478d169d05248ed9d50a7c926d5beb/6.7441284,73.1663432', 'Hanimaadhoo');
    

/**
 * Function 
 */
function forcast($url, $location){
    //$url = 'https://api.darksky.net/forecast/d0478d169d05248ed9d50a7c926d5beb/4.1755,73.5093'; 
    $data = file_get_contents($url); 
    $obj = json_decode($data, true);

    $db = new Database(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    foreach($obj['daily']['data'] as $row){
        //var_dump($row);
        $time = $row['time'];
        $summary = $row['summary'];
        $icon = $row['icon'];
        $humidity = $row['humidity'];
        $humidity = $humidity * 100;
        $temperatureHigh = $row['temperatureHigh'];
        $temperatureLow = $row['temperatureLow'];

        $meantemp = ($temperatureHigh + $temperatureLow) / 2;

        $unixToDate = unxtodateF($time, 'Y-m-d H:i');

    /**
     * Check for repeated rows before inserting to the weather_forcast table
     */
        $rowcount = $db->query("SELECT COUNT(*) FROM public.weather_forcast WHERE wfdate_unix = '".trim($time)."' AND wlocation = '".trim($location)."' ");
        $rowcount = $db->execute();
        $rowcount = $db->fetchColumn();

        if($rowcount < 1){
            /**
             * If the data is new, insert into the weather_forcast table
             */
            $insert = $db->query("INSERT INTO public.weather_forcast (summary, icon, humidity, temperature, wfdate_unix, wfdate, wlocation) VALUES (
                :summary,
                :icon,
                :humidity,
                :temperature,
                :wfdate_unix,
                :wfdate,
                :wlocation
            )");
            
            $insert = $db->bind(':summary', trim($summary));
            $insert = $db->bind(':icon', trim($icon));
            $insert = $db->bind(':humidity', trim($humidity));
            $insert = $db->bind(':temperature', trim($meantemp));
            $insert = $db->bind(':wfdate_unix', trim($time));
            $insert = $db->bind(':wfdate', trim($unixToDate));
            $insert = $db->bind(':wlocation', trim($location));

            $insert = $db->execute();
        }


    }
}


?>