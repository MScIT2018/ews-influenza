<?php
/**
 * Set php error reporting
 * display_errors: set display_errors to 1 to display error (only for debugging), to turn off error displays set display_errors 0 or NULL
 * display_errors: set 1 to display othe errors (only for debugging), to turn off error displays set display_startup_errors 0 or NULL
 * error_reporting: set reporting level E_ALL for all the error (only for debugging), to turn off set error_reporting 0 or NULL
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/**
 * The base URL of the project
 * This is the url of the application. Change the url to your project url or domain name
 * The constant ROOT_PATH is used to include css, js, images as a base url
 */
define("ROOT_PATH", "http://ewsi:801");
define("DOCUMENT_ROOT", $_SERVER["DOCUMENT_ROOT"]);

/**
 * Database connection properties
 * DB_HOST: the server host name
 * DB_USER: the database user
 * DB_PASS: the database user password
 * DB_NAME: name the database
 */
define("DB_HOST", "localhost");
define("DB_USER", "postgres");
define("DB_PASS", "");
define("DB_NAME", "itmaster");

