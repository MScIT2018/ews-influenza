/**
 * Load the live weather 
 */
function loadweather(){
    $('#load_live_weather').load('views/liveweather.php');
}

$(document).ready(function(e) {
    loadweather();
});

setInterval(function(){
    loadweather() 
}, 3600000);


/**
 * Share the alerts to Telegram
 */
$('.alert_share').click(function(e) {

    var id = $(this).data('id');

    $('#prg'+id).show();
    var ntdate = $('.ntdate'+id).text();
    var ntlocation = $('.ntlocation'+id).text();
    var msg = 'Influenza Alert: \n'+ntdate +'\n'+ ntlocation;
    
    $.post({
        url: "https://api.telegram.org/bot605257302:AAGej34UVa_684E__8NGdWCwPn97cwKopeA/sendMessage?chat_id=-301486923",
        data: { "text": msg },
        success: function(data){
            $.notify({
                // options
                title: '',
                message: 'Telegram alert sent' 
            },{
                // settings
                element: 'body',
                position: null,
                type: "success",
                allow_dismiss: true,
                newest_on_top: false,
                showProgressbar: false,
                placement: {
                    from: "top",
                    align: "right"
                }
            });

          $('#prg'+id).hide();
        }
      });
});







