<?php
include ('../functions.php');
/**
* Get live weather data from darksky.net using free API
*/

//$url = 'https://api.darksky.net/forecast/d0478d169d05248ed9d50a7c926d5beb/4.1755,73.5093'; 
$url = 'https://api.darksky.net/forecast/e4a82aab3cc5985319dd096a0ae1a857/4.1755,73.5093'; 

$data = file_get_contents($url); 
$obj = json_decode($data, true);


$weatherDescription= $obj['currently']['summary'];
$weatherConditionId = $obj['currently']['icon'];
$temp = $obj['currently']['temperature'];
$pressure = $obj['currently']['pressure'];
$humidity = $obj['currently']['humidity'];

$windSpeed = $obj['currently']['windSpeed'];
$windDeg = $obj['currently']['windBearing'];

$dataTime = $obj['currently']['time'];
$visibility = $obj['currently']['visibility'];
$cloudCover = $obj['currently']['cloudCover'];


$sunrise = '';
$sunset = '';
?>
          <div class="weather_title">Male' Maldives <?= unixToDate($dataTime) ?></div>
          <div class="box-body border-radius-none">
                  
                    <!--weather icon-->
                    <div class="c_weather_icon">
                        <?= weatherIcon($weatherConditionId); ?>
                    </div>
                    
                    <div class="c_weather_info">
                        <h3 class="weather_deg"><?= FahrenheitToDegree($temp) ?><sup>°C</sup></h3>
                        <h3><?= ucwords($weatherDescription) ?></h3>
                    </div>

                    <div class="c_weather_info">
                        <div class="wind_speed"><strong>Wind: </strong> <?= $windSpeed ?> m/s </div>
                        <?= $windDeg.'&#176; '.windCardinals($windDeg) ?>
                        <i class="wi wi-sunrise color_b"></i><br>
                    </div>
                    <div class="clear"></div>

                </div>
  
          <!-- /.box-body -->
          <div class="box-footer no-border" style="border-right: 1px solid #f4f4f4">
            <div class="row">
              <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
              <i class="wi wi-humidity color_b"></i><br>
                <div class="knob-label weather_d_lbl">
                Humidity  <?= $humidity * 100 ?> %
                </div>
              </div>
              <!-- ./col -->
              <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
              <i class="fa fa-binoculars color_b" aria-hidden="true"></i><br>
                <div class="knob-label weather_d_lbl">
                Visibility <?= $visibility ?> Km
                </div>
              </div>
              <!-- ./col -->
              <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
              <i class="wi wi-cloudy color_b"></i>
<br>
                <div class="knob-label weather_d_lbl">
                  Clud cover <?= $cloudCover ?>
                </div>
              </div>
              <!-- ./col -->
            </div>
            <!-- /.row -->
          </div>