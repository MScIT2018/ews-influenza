<?php
require_once('config.php');
/**
 * Get URL parm for routing
 */
$controller = '';
function validate_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  $data = htmlentities($data, ENT_QUOTES);
  return $data;
}

if(isset($_GET['controller'])){
	$controller =  validate_input($_GET['controller']);
}

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Early Warning System for mass spread of influenza based on weather data in Maldives</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?= ROOT_PATH ?>/dist/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= ROOT_PATH ?>/dist/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?= ROOT_PATH ?>/dist/bower_components/Ionicons/css/ionicons.min.css">

  <!-- weather icons -->
  <link rel="stylesheet" href="<?= ROOT_PATH ?>/dist/css/weather-icons-wind.min.css">
  <link rel="stylesheet" href="<?= ROOT_PATH ?>/dist/css/weather-icons.min.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="<?= ROOT_PATH ?>/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="<?= ROOT_PATH ?>/dist/css/skins/skin-blue.min.css">
  <!-- Custom style sheet -->
  <link rel="stylesheet" href="<?= ROOT_PATH ?>/dist/css/style.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<script src="<?= ROOT_PATH ?>/dist/bower_components/chart.js/Chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini fixed">
<div class="wrapper">

<?php
include('includes/header.php');
include('includes/slidebar.php');


?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <?php
    /**
     * Manage routing and contollers
     */
    if($controller == ''){
      $controller =  'dashboard';
    }
    $dir = 'views/'.$controller;
      if(empty($controller)){
        include('views/dashboard/index.php');
      }else{
        //check if directory exist
        if (!is_dir($dir)) {
          //header('Location: https://*******');
        }else{
          include('views/'.$controller.'/index.php');
        }
      }	
    ?>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php
include('includes/footer.php');
?>

 
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="<?= ROOT_PATH ?>/dist/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= ROOT_PATH ?>/dist/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= ROOT_PATH ?>/dist/js/adminlte.min.js"></script>
<script src="<?= ROOT_PATH ?>/dist/js/bootstrap-notify.min.js"></script>
<script src="<?= ROOT_PATH ?>/dist/js/custom.js"></script>

</body>
</html>