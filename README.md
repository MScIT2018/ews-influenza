# Early Warning System for mass spread of influenza based on weather data in Maldives

Early Warning System for mass spread of influenza based on weather data in Maldives

## Getting Started
The system would enable healthcare professionals of the Maldives to take precautionary measures for a possible influenza outbreaks during flu season. The system analyses weather data to find possible outbreaks that could occur due to change in weather. As weather can be predicted, the prediction data could be used by the system to foresee an outbreak of influenza viruses.

### Prerequisites

* Ubuntu Server 16.04
* Apache Server
* PHP 7.0 or later
* PostgreSQL 9.3 or later
* install PostgreSQL driver

```
apt-get install php-pgsql
```
enable php mod_rewrite module

```
sudo a2enmod rewrite
```

install php-curl
```
sudo apt-get install php-curl
```


### Installing

```
git clone https://gitlab.com/MScIT2018/ews-influenza.git
```

Edit config.php file
Change the ROOT_PATH to your domain or ip

Setup database
```
define("DB_HOST", "localhost");  // change to your server hostname 
define("DB_USER", "postgres");   // The database user
define("DB_PASS", "");           // The database password
define("DB_NAME", "itmaster");   //The database name
```

Run the cron jobs
```
sudo crontab –e
* */2 * * * /usr/bin/php /var/www/html/services/weather_forecast.php
* */2 * * * /usr/bin/php /var/www/html/services/influenza_alert_check.php
```

Change the file permissions 
```
chmod 755 /var/www/html/services
```

## Built With

* [Adminlte](https://adminlte.io/) - The template used
* [Darksky](https://darksky.net/) - The weather API


## Authors
* Hassan Mohamed
* Mohamed Ismail 
* Ahmed Mishal
* Ali Shiyam
* Thoha Abubakur

