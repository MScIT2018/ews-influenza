<?php
/**
* Get live weather data from darksky.net using free API
*/
$url = 'https://api.darksky.net/forecast/d0478d169d05248ed9d50a7c926d5beb/4.1755,73.5093'; 
$data = file_get_contents($url); 
$obj = json_decode($data, true);

var_dump($obj); 

$weatherConditionId = $obj['weather']['0']['id'];
$weatherMain = $obj['weather']['0']['main'];
$weatherDescription= $obj['currently']['summary'];

$temp = $obj['main']['temp'];
$pressure = $obj['main']['pressure'];
$humidity = $obj['main']['humidity'];
$temp_min = $obj['main']['temp_min'];
$temp_max = $obj['main']['temp_max'];

$windSpeed = $obj['wind']['speed'];
$windDeg = $obj['wind']['deg'];

$clouds = $obj['clouds']['all'];

$dataTime = $obj['currently']['time'];

$sunrise = $obj['sys']['sunrise'];
$sunset = $obj['sys']['sunset'];


echo $dataTime;
